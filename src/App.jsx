import { useState } from "react";
import maxresdefault from '../public/img/maxresdefault.jpg';

function App() {
  const Bob = {
    reponses: [
       // Question
    { regex: /^[A-Z][a-z\s-]+\s\?/, reponse: "Bien sûr." }, 
    // CRIER
    { regex: /^[A-Z\s]+[A-Z]$/, reponse: "Whoa, calmos !" },
    // CRIER une question[A-Z]\w+\s\?
    { regex: /[A-Z\s]*[?]/, reponse: "Calmez-vous, je sais ce que je fais !" },
    // Aucun texte
    { regex: /^$/, reponse: "Bien, on fait comme ça !" },
    // Autre cas
    { regex: /.*/, reponse: "Peu importe !" }

    ],
    repondre(userMessage) {
      for (const { regex, reponse } of this.reponses) {
        if (regex.test(userMessage)){
          return reponse;
        }
      }
    }
  };

  const [inputValue, setInputValue] = useState('');
  const [bobResponse, setBobResponse] = useState('GGGggggggghhhh');

  const handleSubmit = (event) => {
    event.preventDefault();
    const userMessage = inputValue.trim();
    const response = Bob.repondre(userMessage);
    setBobResponse(response);
    setInputValue(''); 
  };

  return (
    <>
      <div className="container">
        <div className="row my-5">
          <div className="col">
            <h1 className="text-center">Teenage Boy Speaking</h1>
          </div>
        </div>
        <div className="row my-3">
          <div className="col-4">
            <img src={maxresdefault} alt="image de max" className="img-fluid" />
          </div>
          <div className="col-8 pt-5">
            <div className="mt-5 thought" id="bobResponse">
              {bobResponse}
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <hr />
          </div>
          <div className="col text-center">
            <form 
              onSubmit={handleSubmit} 
              className="form-inline justify-content-center"
            >
              <div className="form-group">
                <input 
                  id="messageInput" 
                  value={inputValue}
                  onChange={(e) => setInputValue(e.target.value)} 
                  className="form-control mr-2" 
                  type="text" 
                  placeholder="Entrez votre texte"
                />
              </div>
              <div className="form-group">
                <input 
                  id="sendMessageButton" 
                  className="btn btn-outline-dark" 
                  type="submit" 
                  value="Parler"
                />
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
