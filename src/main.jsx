import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import "./stylesheets/application.sass";
import * as bootstrap from "bootstrap";
import "./stylesheets/app.scss";

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)
