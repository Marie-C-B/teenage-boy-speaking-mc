import { render, screen, fireEvent } from "@testing-library/react";
import App from "../src/App";

describe("UI test", () => {

  beforeEach(() => {
    render(<App />);
  });

  it("should display the page title", () => {
    const heading = screen.getByRole("heading", { name: "Teenage Boy Speaking" });
    expect(heading).toBeInTheDocument();
  });

  it("should have input", () => {
    const input = screen.getByPlaceholderText("Entrez votre texte");
    expect(input).toBeInTheDocument();
  });

  it("should have a submit button", () => {
    const button = screen.getByRole("button", { name: "Parler" });
    expect(button).toBeInTheDocument();
  });

  it("should update the response text based on the input 'Comment allez-vous ?'", () => {
    const input = screen.getByPlaceholderText("Entrez votre texte");
    const button = screen.getByRole("button", { name: "Parler" });

    fireEvent.change(input, { target: { value: "Comment allez-vous ?" } });
    fireEvent.click(button);

    expect(screen.getByText("Bien sûr.")).toBeInTheDocument();
  });

  it("should update the response text based on the input 'JE CRIE FORT'", () => {
    const input = screen.getByPlaceholderText("Entrez votre texte");
    const button = screen.getByRole("button", { name: "Parler" });

    fireEvent.change(input, { target: { value: "JE CRIE FORT" } });
    fireEvent.click(button);

    expect(screen.getByText("Whoa, calmos !")).toBeInTheDocument();
  });

  it("should update the response text based on the input 'MA QUESTION UPPERCASE ?'", () => {
    const input = screen.getByPlaceholderText("Entrez votre texte");
    const button = screen.getByRole("button", { name: "Parler" });

    fireEvent.change(input, { target: { value: "MA QUESTION UPPERCASE ?" } });
    fireEvent.click(button);

    expect(screen.getByText("Calmez-vous, je sais ce que je fais !")).toBeInTheDocument();
  });

  it("should update the response text based on the input ''", () => {
    const input = screen.getByPlaceholderText("Entrez votre texte");
    const button = screen.getByRole("button", { name: "Parler" });

    fireEvent.change(input, { target: { value: "" } });
    fireEvent.click(button);

    expect(screen.getByText("Bien, on fait comme ça !")).toBeInTheDocument();
  });

  it("should update the response text based on the input 'heuuuuu'", () => {
    const input = screen.getByPlaceholderText("Entrez votre texte");
    const button = screen.getByRole("button", { name: "Parler" });

    fireEvent.change(input, { target: { value: "heuuuuu" } });
    fireEvent.click(button);

    expect(screen.getByText("Peu importe !")).toBeInTheDocument();
  });

  it("should display the initial response text", () => {
    expect(screen.getByText("GGGggggggghhhh")).toBeInTheDocument();
  });

  it("should clear the input after form submission", () => {
    const input = screen.getByPlaceholderText("Entrez votre texte");
    const button = screen.getByRole("button", { name: "Parler" });

    fireEvent.change(input, { target: { value: "some text" } });
    fireEvent.click(button);

    expect(input.value).toBe("");
  });
});
